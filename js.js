$(document).ready(function () {

    let res;
    var data = null;
    var roundCounter = 0;
    let tmp = 1;
    // обработчик события загрузки файла
    document.getElementById('txtFileUpload').addEventListener('change', upload, false);

    // проверка поддержки браузером возможности чтения файла
    function browserSupportFileUpload() {
        var isCompatible = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            isCompatible = true;
        }
        return isCompatible;
    };

    // чтение файла и запись в массив
    function upload(evt) {

        if (!browserSupportFileUpload()) {
            console.log('этот браузер не поддерживается!');
        } else {

            var file = evt.target.files[0];
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function (event) {
                var csvData = event.target.result;
                data = $.csv.toArrays(csvData);

                if (data && data.length > 0) {
                    console.log('загружено -' + data.length + '- строк');

                } else {
                    console.log('нечего загрузить');
                }
            };
            reader.onerror = function () {
                console.log(file.fileName + 'не читается ');
            };
        }
    }

    function getRandNum(data) {
        res = Math.round(Math.random() * data.length);
        return res;
    }

    function getListOfNuns(data) {
        resultArray = [];
        count = document.getElementById("count").value;
        roundCounter++;
        while (resultArray.length != count) {

            getRandNum(data);
            let d = data[res];

            if (d[2] != "winner") {
                d.push("winner");
                d.push(roundCounter);
                resultArray.push(d);
            } else {

                continue;

            }
        }
        console.log(resultArray);
        
        tmp++;
        return resultArray;

    }

    function getData() {
        getListOfNuns(data);

        var div = $(".round");
        var ul = $(".round-list");

        for (elem in resultArray) {
            var li = document.createElement('li');
            var span = document.createElement('span');
            $(span).appendTo(li);
            $(span).text(resultArray[elem][0]);
            $(li).appendTo(ul);
            $(ul).appendTo(div);

        }


        ul.addClass("filled");

    }

    function gameStart(roundCounter) {

        if ($(".round-list").hasClass("filled")) {
            $("li").detach();
            $(".round-list").removeClass("filled");
            $(".loader").removeClass("hidden");
            $(".round").addClass("hidden");



        } else {
            $(".settings").addClass("hidden");
            $(".loader").addClass("hidden");
            $(".round").removeClass("hidden");
            getData();

        }
    }

    function endGame() {
        $(".finale").removeClass("hidden");
        $(".round").addClass("hidden");
        $(".settings").addClass("hidden");
        $(".loader").addClass("hidden");

    }


    $("#export").click(function () {
        // сборка файла из двумерного массива 
        // каждый элемент массива разделяется ";" переносом строки "\n" 
        var csvContent = '';
        data.forEach(function (infoArray, index) {
            dataString = infoArray.join(';');
            csvContent += index < data.length ? dataString + '\n' : dataString;
        });

        // The download function takes a CSV string, the filename and mimeType as parameters
        // Scroll/look down at the bottom of this snippet to see how download is called
        var download = function (content, fileName, mimeType) {
            var a = document.createElement('a');
            mimeType = mimeType || 'application/octet-stream';

            if (navigator.msSaveBlob) { // IE10
                navigator.msSaveBlob(new Blob([content], {
                    type: mimeType
                }), fileName);
            } else if (URL && 'download' in a) { //html5 A[download]
                a.href = URL.createObjectURL(new Blob([content], {
                    type: mimeType
                }));
                a.setAttribute('download', fileName);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            } else {
                location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
            }
        }

        download(csvContent, 'dowload.csv', 'text/csv;encoding:utf-8');
    });

    document.addEventListener('keyup', (event) => {
        var name = event.key;
        switch (name) {
            case "ArrowLeft":
                $(".settings").removeClass("hidden");
                $(".finale").addClass("hidden");

                break;
            case "ArrowRight":
                nextRound();
                break;
            case "ArrowUp":
                $(".loader").removeClass("hidden");
                gameStart(roundCounter);
                $(".counter-item").text(roundCounter);
                break;
            case "ArrowDown":
                endGame();
                break;
        }


    });

    /*function leavesAnimation() {
        leaf = document.createElement('img');
        var leafRand = Math.round(Math.random() * 2000);
        var randDelay = Math.random() * 7.0;
        let style = "animation-delay: " + randDelay + "s;";
        $(leaf).attr("src", "img/logo-loader.png");
        $(leaf).attr("alt", "autumn leaf");
        $(leaf).attr("style", style);
        $(leaf).css({
            "left": leafRand,
        });
        $(leaf).appendTo(".parallax-container");
        console.log(style);

    }

    for (let i = 0; i < 40; i++) {
        leavesAnimation()

    }*/
    /*$('.parallax-container').particleground({
        dotColor: '#ffffff',
    lineColor: '#000',
        lineWidth: 0,
        curvedLines: false
        
    });*/
});
